import 'package:audio_book/Screens/favrouit_screen.dart';
import 'package:flutter/material.dart';

class SideBar extends StatefulWidget {
  const SideBar({Key? key}) : super(key: key);

  @override
  State<SideBar> createState() => _SideBarState();
}

class _SideBarState extends State<SideBar> {
  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;

    return Drawer(
      child: Stack(
        children: [
          Image.network("https://files.oyebesmartest.com/uploads/preview/redmi-note-8-pro-wallpaper-full-hd-portrait-(6)l959swcmbc.jpg",height: height,fit: BoxFit.cover,),
          Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            const Padding(
              padding: EdgeInsets.only(left: 30, top: 70),
              child: CircleAvatar(
                radius: 60,
                backgroundColor: Colors.white,
              ),
            ),
            const SizedBox(height: 40),
            listWidget(
                text: "Favorite", next: Icons.heart_broken, color: Colors.red,onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return FavouriteScreen();
                  },));
                },),
            listWidget(text: "Share App", next: Icons.share, color: Colors.black),
            listWidget(text: "Rate us", next: Icons.star, color: Colors.amber),
            listWidget(
                text: "Privacy Policy",
                next: Icons.privacy_tip,
                color: Colors.white),
            listWidget(
                text: "More App", next: Icons.app_blocking, color: Colors.white),
          ]),
        ],
      ),
    );
  }

  Widget listWidget({String? text, IconData? next, Color? color,VoidCallback? onTap}) {
    return GestureDetector(
      onTap: onTap,
      child: ListTile(
        title: Text(
          text!,
          style: const TextStyle(fontSize: 18,color: Colors.white,fontWeight: FontWeight.bold),
        ),
        leading: Icon(
          next!,
          color: color,
          size: 35,
        ),
      ),
    );
  }
}
