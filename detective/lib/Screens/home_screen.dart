import 'dart:async';
import 'dart:convert';
import 'package:audio_book/Notification/local_notification.dart';
import 'package:audio_book/Screens/audio_list.dart';
import 'package:audio_book/Widget/search_widget.dart';
import 'package:audio_book/Widget/sidebar.dart';
import 'package:audio_book/constance/AppColor.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';

import '../common/utils.dart';
import 'admob_helper.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  var data;
  String query = "";
  NativeAd? _ad;
  bool isAdLoaded = false;
  FirebaseRemoteConfig remoteConfig = FirebaseRemoteConfig.instance;
  List<String> imageList = [
    "assets/images/Sad_Cypress.png",
    "assets/images/The_Unexpected_Guest.png",
    "assets/images/Murder_Is_Easy.png",
    "assets/images/Elephants_Can_Remember.png",
    "assets/images/The_Mystery_of_the_Blue_Train.png",
    "assets/images/Death_Comes_As_The_End.png",
    "assets/images/Death_Comes_As_The_End.png",
    "assets/images/The_Sittaford_Mystery.png",
    "assets/images/Hickory_Dickory_Dock.png",
    "assets/images/Spider_s_Web.png",
    "assets/images/The_Secret_Of_Chimneys.png",
    "assets/images/The_Monogram_Murders.png",
    "assets/images/The_ABC_Murders.png",
    "assets/images/One_Two_Buckle_My_Shoe.png",
    "assets/images/The_Secret_Adversary.png",
    "assets/images/N_Or_M.png",
    "assets/images/By_The_Pricking_Of_My_Thumbs.png",
    "assets/images/The_Capture_of_Cerberus_and_The_Incident_of_the_Dog_s_Ball.png",
    "assets/images/By_The_Pricking_Of_My_Thumbs.png",
    "assets/images/By_The_Pricking_Of_My_Thumbs.png",
    "assets/images/By_The_Pricking_Of_My_Thumbs.png",
    "assets/images/By_The_Pricking_Of_My_Thumbs.png",
  ];

  List<Map<String, dynamic>> originalData = [];
  List<Map<String, dynamic>> filterData = [];

  @override
  initState() {
    _ad = NativeAd(
      adUnitId: AdHelper.rewardedAdUnitId,
      factoryId: 'listTile',
      request: AdRequest(),
      listener: NativeAdListener(
        // Called when an ad is successfully received.
        onAdLoaded: (Ad ad) {
          var _add = ad as NativeAd;
          print("**** AD ***** ${_add.responseInfo}");
          setState(() {
            _ad = _add;

            isAdLoaded = true;
          });
        },
        // Called when an ad request failed.
        onAdFailedToLoad: (Ad ad, LoadAdError error) {
          // Dispose the ad here to free resources.
          ad.dispose();
          print('Ad load failed (code=${error.code} message=${error.message})');
        },
        // Called when an ad opens an overlay that covers the screen.
        onAdOpened: (Ad ad) => print('Ad opened.'),
        // Called when an ad removes an overlay that covers the screen.
        onAdClosed: (Ad ad) => print('Ad closed.'),
        // Called when an impression occurs on the ad.
        onAdImpression: (Ad ad) => print('Ad impression.'),
        // Called when a click is recorded for a NativeAd.
        onAdClicked: (Ad ad) => print('Ad clicked.'),
      ),
    );
    _ad!.load();
    print(_ad);
    BannerAd(
      adUnitId: AdHelper.bannerAdUnitId,
      request: AdRequest(),
      size: AdSize.fullBanner,
      listener: BannerAdListener(
        onAdLoaded: (ad) {
          setState(() {
            _bannerAd = ad as BannerAd;
          });
        },
        onAdFailedToLoad: (ad, err) {
          print('Failed to load a banner ad: ${err.message}');
          ad.dispose();
        },
      ),
    ).load();
    // TODO: implement initState
    super.initState();
    fetchData();
    var rawData = remoteConfig.getAll()["detective_audio_book"];
    var map = json.decode(rawData?.asString() ?? "");
    for (var item in map["data"]) {
      originalData.add(item);
    }
    print(originalData.length);
    filterData = originalData;
    print(filterData[1]);
    NotificationData.showNotification();
    initConnectivity();

    _connectivitySubscription =
        _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
  }

  void dispose() {
    _connectivitySubscription.cancel();
    super.dispose();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initConnectivity() async {
    late ConnectivityResult result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      // developer.log('Couldn\'t check connectivity status', error: e);
      return;
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return Future.value(null);
    }

    return _updateConnectionStatus(result);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    setState(() {
      _connectionStatus = result;
    });
  }

  ConnectivityResult _connectionStatus = ConnectivityResult.none;
  final Connectivity _connectivity = Connectivity();
  late StreamSubscription<ConnectivityResult> _connectivitySubscription;
  BannerAd? _bannerAd;

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Scaffold(
        drawer: const SideBar(),
        appBar: AppBar(
            backgroundColor: AppColor.primaryColor,
            centerTitle: true,
            title: const Text(
              "Detective Audio Storybook",
            )),
        body: Stack(
          children: [
            FutureBuilder<FirebaseRemoteConfig>(
              future: setupRemoteConfig(),
              builder: (BuildContext context,
                  AsyncSnapshot<FirebaseRemoteConfig> snapshot) {
                return snapshot.hasData
                    ? listOfAudiobook(
                        width: width,
                        height: height,
                        remoteConfig: snapshot.requireData)
                    : Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SpinKitCircle(
                            color: AppColor.primaryColor,
                            size: 50.0,
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            "Loading",
                            style:
                                const TextStyle(color: AppColor.primaryColor),
                          ),
                        ],
                      );
              },
            ),
            _connectionStatus.toString().contains("none")
                ? internet()
                : Container(),
            Positioned(
              child: _bannerAd != null
                  ? Container(
                      width: _bannerAd!.size.width.toDouble(),
                      height: _bannerAd!.size.height.toDouble(),
                      child: AdWidget(ad: _bannerAd!),
                    )
                  : Container(),
              bottom: 0,
            )
          ],
        ));
  }

  listOfAudiobook({width, height, required FirebaseRemoteConfig remoteConfig}) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 5, right: 5),
          child: buildSearch(),
        ),
        const SizedBox(
          height: 0,
        ),
        _ad != null && isAdLoaded == true
            ? Container(
                width: 500,
                height: 90,
                alignment: Alignment.center,
                child: AdWidget(ad: _ad!),
              )
            : Text("hi"),
        _itemList(width, height, remoteConfig: remoteConfig)
      ],
    );
  }

  Expanded _itemList(double width, double height,
      {required FirebaseRemoteConfig remoteConfig}) {
    return Expanded(
      child: ListView.builder(
        itemCount: filterData.length,
        physics: const BouncingScrollPhysics(),
        itemBuilder: (context, index) {
          var item = filterData[index];
          // bookNameList = duplicateGetAllData[index]["book_name"];
          // print(bookNameList);
          return Padding(
            padding: const EdgeInsets.only(right: 25, bottom: 10),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  children: [
                    SizedBox(height: 15),
                    Container(
                      margin: const EdgeInsets.only(top: 5),
                      padding: const EdgeInsets.only(
                          left: 15, right: 15, top: 10, bottom: 10),
                      decoration: const BoxDecoration(
                          color: Color(0xffBC6621),
                          borderRadius: BorderRadius.only(
                              topRight: Radius.circular(20),
                              bottomRight: Radius.circular(20))),
                      child: Text(
                        "${index + 1}",
                        style: const TextStyle(color: TextColor.textColorWhite),
                      ),
                    ),
                  ],
                ),
                Column(
                  children: [
                    GestureDetector(
                      onTap: () {
                        Navigator.push(context, MaterialPageRoute(
                          builder: (context) {
                            return AudioList(
                                audiobook_name: item["book_name"],
                                storyLink: item["audio_links"],
                                image: imageList[index]);
                          },
                        ));
                      },
                      child: Stack(
                        children: [
                          Container(
                            margin: const EdgeInsets.all(5),
                            height: height * .2,
                            width: width * .72,
                            decoration: BoxDecoration(
                              boxShadow: [
                                BoxShadow(
                                    offset: Offset(0, 4),
                                    color: Colors.black,
                                    blurRadius: 8)
                              ],
                              color: Colors.blue,
                              borderRadius: BorderRadius.circular(5),
                            ),
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(5),
                              child: Image.asset(
                                imageList[index],
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                          // Positioned(
                          //   bottom: 0,
                          //   right: 0,
                          //   left: 0,
                          //   child: Container(
                          //     padding: const EdgeInsets.only(bottom: 10, top: 10),
                          //     margin: const EdgeInsets.all(5),
                          //     // height: 30,
                          //     alignment: Alignment.center,
                          //     decoration: BoxDecoration(
                          //         color: Colors.black.withOpacity(0.6),
                          //         borderRadius: const BorderRadius.only(
                          //             bottomLeft: Radius.circular(10),
                          //             bottomRight: Radius.circular(10))),
                          //     child: Padding(
                          //       padding: const EdgeInsets.symmetric(horizontal: 10),
                          //       child: Text(
                          //         item["book_name"],
                          //         style: const TextStyle(
                          //             color: TextColor.textColorWhite,
                          //             fontWeight: FontWeight.bold,
                          //             fontSize: 18),
                          //       ),
                          //     ),
                          //   ),
                          // )
                        ],
                      ),
                    ),
                  ],
                )
              ],
            ),
          );
        },
      ),
    );
  }

  Widget buildSearch() =>
      SearchWidget(text: "", onChanged: searchBook, hintText: "");

  void searchBook(String query1) {
    final books1 = originalData.where((element) {
      final titleLower = element["book_name"].toLowerCase();
      final searchLower = query1.toLowerCase();
      return titleLower.contains(searchLower);
    }).toList();

    setState(() {
      filterData = books1;
    });
  }

  Future<FirebaseRemoteConfig> setupRemoteConfig() async {
    await remoteConfig.setConfigSettings(RemoteConfigSettings(
      fetchTimeout: const Duration(hours: 1),
      minimumFetchInterval: const Duration(minutes: 1),
    ));
    setState(() {});
    return remoteConfig;
  }

  Future<void> fetchData() async {
    await remoteConfig.fetchAndActivate();
  }
}
