import 'package:flutter/material.dart';

class AppColor {
  static const primaryColor = Color(0xff9E0000);
  static const darkPrimary = Color(0xffd95555);
  static const accentColor = Color(0xff2aa180);
}

class TextColor {
  static const textColorWhite = Color(0xffffffff);
  static const textColorBlack = Color(0xff424242);
}