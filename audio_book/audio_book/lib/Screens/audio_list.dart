import 'package:audio_book/Screens/audio_play.dart';
import 'package:audio_book/constance/AppColor.dart';
import 'package:flutter/material.dart';
import 'package:just_audio/just_audio.dart';

class AudioList extends StatefulWidget {
  String? audiobook_name;
  List<dynamic>? storyLink;
  String? image;

  List<String>? storyID;

  AudioList({this.audiobook_name, this.storyLink, this.image, this.storyID});

  @override
  State<AudioList> createState() => _AudioListState();
}

class _AudioListState extends State<AudioList> {
  final AudioPlayer _audioPlayer = AudioPlayer();
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Scaffold(
        appBar: AppBar(
          backgroundColor: AppColor.primaryColor,
          title: Text(widget.audiobook_name ?? ""),
        ),
        body: Padding(
          padding: const EdgeInsets.only(top: 20),
          child: ListView.builder(
            itemCount: widget.storyLink!.length,
            physics: const BouncingScrollPhysics(),
            itemBuilder: (context, index) {
              return GestureDetector(
                onTap: () {
                  print("audiolidt00");
                  print(widget.storyLink![index]["id"]);
                  Navigator.push(context, MaterialPageRoute(
                    builder: (context) {
                      return AudioPlay(
                        image: widget.image,
                        storyLink: widget.storyLink,
                        songUrl: widget.storyLink![index]["link"],
                        storyName: widget.audiobook_name,
                        storyID: widget.storyLink![index]["id"],
                      );
                    },
                  ));
                },
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      margin: const EdgeInsets.only(top: 5),
                      padding: const EdgeInsets.only(
                          left: 20, right: 20, top: 10, bottom: 10),
                      decoration: const BoxDecoration(
                          color: AppColor.primaryColor,
                          borderRadius: BorderRadius.only(
                              topRight: Radius.circular(20),
                              bottomRight: Radius.circular(20))),
                      child: Text(
                        "${index + 1}",
                        style: const TextStyle(color: Colors.white),
                      ),
                    ),
                    Stack(
                      children: [
                        Container(
                          height: height * .18,
                          width: width * .8,
                          margin: const EdgeInsets.all(5),
                          decoration: BoxDecoration(
                            color: Colors.blue,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: ClipRRect(
                              borderRadius: BorderRadius.circular(10),
                              child: Image.asset(
                                widget.image ?? "",
                                fit: BoxFit.cover,
                              )),
                        ),
                        Positioned(
                            bottom: 0,
                            right: 0,
                            left: 0,
                            child: Container(
                              padding:
                                  const EdgeInsets.only(bottom: 10, top: 10),
                              margin: const EdgeInsets.all(5),
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                  color: Colors.black.withOpacity(0.6),
                                  borderRadius: const BorderRadius.only(
                                      bottomLeft: Radius.circular(10),
                                      bottomRight: Radius.circular(10))),
                              child: Text(
                                "Story ${index + 1}",
                                style: const TextStyle(
                                    color: TextColor.textColorWhite,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18),
                              ),
                            ))
                      ],
                    ),
                  ],
                ),
              );
            },
          ),
        ));
  }
}
