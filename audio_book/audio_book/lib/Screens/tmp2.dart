// import 'dart:async';

// import 'package:assets_audio_player/assets_audio_player.dart';
// import 'package:audio_book/Screens/PlayingControls.dart';
// import 'package:audio_book/Screens/seek.dart';
// import 'package:audio_book/Screens/songs_selector.dart';
// import 'package:audio_book/Widget/controll_button.dart';
// import 'package:audio_book/common/common.dart';
// import 'package:audio_book/constance/AppColor.dart';
// import 'package:audio_book/model/database%20model.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_neumorphic/flutter_neumorphic.dart';
// // import 'package:just_audio/just_audio.dart';
// // import 'package:just_audio_background/just_audio_background.dart';
// import 'package:rxdart/rxdart.dart';
// import 'package:sqflite/sqflite.dart';

// class AudioPlay extends StatefulWidget {
//   String? storyName;
//   List<dynamic>? storyLink;
//   String? songUrl;
//   String? image;
//   String? storyID;

//   AudioPlay(
//       {this.storyName, this.songUrl, this.image, this.storyID, this.storyLink});

//   @override
//   State<AudioPlay> createState() => _AudioPlayState();
// }

// class _AudioPlayState extends State<AudioPlay> with WidgetsBindingObserver {
//   //final _player = AudioPlayer();
//   bool isFavorite = false;
//   Database? database;
//   String qry = "";
//   List<Map> list = [];

//   @override
//   final AssetsAudioPlayer audioPlayer = AssetsAudioPlayer();

//   late AssetsAudioPlayer _assetsAudioPlayer;
//   final List<StreamSubscription> _subscriptions = [];
//   final audios = <Audio>[];
//   void getAllData() {
//     ModelDatabase.createDataBase().then((value) async {
//       database = value;
//       qry = "select * from addInCart WHERE storyID = ${widget.storyID}";
//       list = await database!.rawQuery(qry);
//       if (list.length > 0) {
//         isFavorite = true;
//       } else {
//         isFavorite = false;
//       }
//       setState(() {});
//     });
//   }

//   @override
//   void initState() {
//     super.initState();
//     print(widget.storyID);
//     _assetsAudioPlayer = AssetsAudioPlayer.newPlayer();
//     //_subscriptions.add(_assetsAudioPlayer.playlistFinished.listen((data) {
//     //  print('finished : $data');
//     //}));
//     //openPlayer();
//     _subscriptions.add(_assetsAudioPlayer.playlistAudioFinished.listen((data) {
//       print('playlistAudioFinished : $data');
//     }));
//     _subscriptions.add(_assetsAudioPlayer.audioSessionId.listen((sessionId) {
//       print('audioSessionId : $sessionId');
//     }));
//     ambiguate(WidgetsBinding.instance)!.addObserver(this);

//     ModelDatabase.createDataBase().then((value) => {database = value});
//     getAllData();

//     for (int i = 0; i < widget.storyLink!.length; i++) {
//       setState(() {
//         audioList.add(widget.storyLink![i]["link"]);

//         audios.add(
//           Audio.network(
//             audioList[i],
//             metas: Metas(
//               id: widget.storyLink![i]["id"],
//               title: widget.storyName,

//               // artist: 'Florent Champigny',
//               // album: 'OnlineAlbum',
//               // image: MetasImage.network('https://www.google.com')
//               image: MetasImage.asset(widget.image!),
//             ),
//           ),
//         );
//       });
//     }
//     openPlayer();
//   }

//   static List audioList = [];
//   void openPlayer() async {
//     await _assetsAudioPlayer.open(
//       Playlist(
//           audios: audios, startIndex: int.parse(widget.storyID.toString()) - 1),
//       showNotification: true,
//       autoStart: false,
//       headPhoneStrategy: HeadPhoneStrategy.pauseOnUnplug,
//       playInBackground: PlayInBackground.enabled,
//     );
//   }

//   @override
//   void dispose() {
//     _assetsAudioPlayer.dispose();
//     print('dispose');
//     super.dispose();
//   }

//   Audio find(List<Audio> source, String fromPath) {
//     return source.firstWhere((element) => element.path == fromPath);
//   }

//   @override
//   Widget build(BuildContext context) {
//     var width = MediaQuery.of(context).size.width;
//     var height = MediaQuery.of(context).size.height;

//     return Scaffold(
//       appBar: AppBar(
//         centerTitle: true,
//         backgroundColor: AppColor.primaryColor,
//         title: Text("Story ${widget.storyName.toString()}"),
//         actions: [
//           Padding(
//               padding: const EdgeInsets.only(right: 20),
//               child: GestureDetector(
//                   onTap: () async {
//                     isFavorite = !isFavorite;
//                     if (isFavorite) {
//                       String qry =
//                           "insert into addInCart (image,storyName,storyLink,storyID) values ('${widget.image}','${widget.storyName.toString()}','${widget.songUrl}','${widget.storyID}')";
//                       await database!.rawInsert(qry);
//                       setState(() {});
//                     } else {
//                       String qry =
//                           "DELETE FROM addInCart WHERE storyID = ${widget.storyID}";
//                       await database!.rawDelete(qry);
//                       setState(() {});
//                     }
//                   },
//                   child: Image.asset(
//                       !isFavorite
//                           ? "assets/images/black_leart.png"
//                           : "assets/images/red_heart.png",
//                       width: 30,
//                       height: 30))),
//         ],
//       ),
//       body: Stack(children: [
//         Scrollbar(
//           child: SingleChildScrollView(
//             child: Padding(
//               padding: const EdgeInsets.symmetric(horizontal: 20),
//               child: Container(
//                   width: width,
//                   height: height,
//                   alignment: Alignment.center,
//                   child: const Text("")),
//             ),
//           ),
//         ),
//         Positioned(
//           bottom: 0,
//           left: 0,
//           right: 0,
//           child: Container(
//             height: height * .23,
//             color: AppColor.primaryColor,
//             child: Column(
//               children: [
//                 Padding(
//                   padding: EdgeInsets.only(top: height * .02),

//                   child: _assetsAudioPlayer.builderCurrent(
//                       builder: (context, Playing? playing) {
//                     return Column(
//                       children: <Widget>[
//                         _assetsAudioPlayer.builderLoopMode(
//                           builder: (context, loopMode) {
//                             return PlayerBuilder.isPlaying(
//                                 player: _assetsAudioPlayer,
//                                 builder: (context, isPlaying) {
//                                   return PlayingControls(
//                                     loopMode: loopMode,
//                                     isPlaying: isPlaying,
//                                     isPlaylist: true,
//                                     onStop: () {
//                                       _assetsAudioPlayer.stop();
//                                     },
//                                     toggleLoop: () {
//                                       _assetsAudioPlayer.toggleLoop();
//                                     },
//                                     onPlay: () {
//                                       _assetsAudioPlayer.playOrPause();
//                                     },
//                                     onNext: () {
//                                       //_assetsAudioPlayer.forward(Duration(seconds: 10));
//                                       _assetsAudioPlayer.next(
//                                           keepLoopMode:
//                                               true /*keepLoopMode: false*/);
//                                     },
//                                     onPrevious: () {
//                                       _assetsAudioPlayer.previous(
//                                           /*keepLoopMode: false*/);
//                                     },
//                                   );
//                                 });
//                           },
//                         ),
//                         _assetsAudioPlayer.builderRealtimePlayingInfos(
//                             builder: (context, RealtimePlayingInfos? infos) {
//                           if (infos == null) {
//                             return SizedBox();
//                           }
//                           //print('infos: $infos');
//                           return Column(
//                             children: [
//                               PositionSeekWidget(
//                                 currentPosition: infos.currentPosition,
//                                 duration: infos.duration,
//                                 seekTo: (to) {
//                                   _assetsAudioPlayer.seek(to);
//                                 },
//                               ),
//                               Row(
//                                 mainAxisAlignment: MainAxisAlignment.center,
//                                 children: [
//                                   NeumorphicButton(
//                                     style: NeumorphicStyle(color: Colors.white),
//                                     onPressed: () {
//                                       _assetsAudioPlayer
//                                           .seekBy(Duration(seconds: -10));
//                                     },
//                                     child: Text('-10'),
//                                   ),
//                                   SizedBox(
//                                     width: 12,
//                                   ),
//                                   NeumorphicButton(
//                                     style: NeumorphicStyle(color: Colors.white),
//                                     onPressed: () {
//                                       _assetsAudioPlayer
//                                           .seekBy(Duration(seconds: 10));
//                                     },
//                                     child: Text('+10'),
//                                   ),
//                                 ],
//                               )
//                             ],
//                           );
//                         }),
//                       ],
//                     );
//                   }),

//                   // _assetsAudioPlayer.builderCurrent(
//                   //     builder: (BuildContext context, Playing? playing) {
//                   //   return SongsSelector(
//                   //     audios: audios,
//                   //     onPlaylistSelected: (myAudios) {
//                   //       _assetsAudioPlayer.open(
//                   //         Playlist(audios: myAudios),
//                   //         showNotification: true,
//                   //         headPhoneStrategy:
//                   //             HeadPhoneStrategy.pauseOnUnplugPlayOnPlug,
//                   //         audioFocusStrategy: AudioFocusStrategy.request(
//                   //             resumeAfterInterruption: true),
//                   //       );
//                   //     },
//                   //     onSelected: (myAudio) async {
//                   //       try {
//                   //         await _assetsAudioPlayer.open(
//                   //           myAudio,
//                   //           autoStart: true,
//                   //           showNotification: true,
//                   //           playInBackground: PlayInBackground.enabled,
//                   //           audioFocusStrategy: AudioFocusStrategy.request(
//                   //               resumeAfterInterruption: true,
//                   //               resumeOthersPlayersAfterDone: true),
//                   //           headPhoneStrategy: HeadPhoneStrategy.pauseOnUnplug,
//                   //           notificationSettings: NotificationSettings(
//                   //               //seekBarEnabled: false,
//                   //               //stopEnabled: true,
//                   //               //customStopAction: (player){
//                   //               //  player.stop();
//                   //               //}
//                   //               //prevEnabled: false,
//                   //               //customNextAction: (player) {
//                   //               //  print('next');
//                   //               //}
//                   //               //customStopIcon: AndroidResDrawable(name: 'ic_stop_custom'),
//                   //               //customPauseIcon: AndroidResDrawable(name:'ic_pause_custom'),
//                   //               //customPlayIcon: AndroidResDrawable(name:'ic_play_custom'),
//                   //               ),
//                   //         );
//                   //       } catch (e) {
//                   //         print(e);
//                   //       }
//                   //     },
//                   //     playing: playing,
//                   //   );
//                   // }),
//                   /*
//                   PlayerBuilder.volume(
//                       player: _assetsAudioPlayer,
//                       builder: (context, volume) {
//                         return VolumeSelector(
//                           volume: volume,
//                           onChange: (v) {
//                             _assetsAudioPlayer.setVolume(v);
//                           },
//                         );
//                       }),
//                    */
//                   /*
//                   PlayerBuilder.forwardRewindSpeed(
//                       player: _assetsAudioPlayer,
//                       builder: (context, speed) {
//                         return ForwardRewindSelector(
//                           speed: speed,
//                           onChange: (v) {
//                             _assetsAudioPlayer.forwardOrRewind(v);
//                           },
//                         );
//                       }),
//                    */
//                   /*
//                   PlayerBuilder.playSpeed(
//                       player: _assetsAudioPlayer,
//                       builder: (context, playSpeed) {
//                         return PlaySpeedSelector(
//                           playSpeed: playSpeed,
//                           onChange: (v) {
//                             _assetsAudioPlayer.setPlaySpeed(v);
//                           },
//                         );
//                       }),
//                    */
//                 )
//               ],
//             ),
//           ),
//         ),
//       ]),
//     );
//   }
// }
import 'package:audio_session/audio_session.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:just_audio/just_audio.dart';
import 'package:just_audio_background/just_audio_background.dart';
// import 'package:just_audio_example/common.dart';
import 'package:rxdart/rxdart.dart';

import '../common/common.dart';

class AudioPlay extends StatefulWidget {
  String? storyName;
  List<dynamic>? storyLink;
  String? songUrl;
  String? image;
  String? storyID;

  AudioPlay(
      {this.storyName, this.songUrl, this.image, this.storyID, this.storyLink});

  @override
  State<AudioPlay> createState() => _AudioPlayState();
}

class _AudioPlayState extends State<AudioPlay> {
  static int _nextMediaId = 0;
  late AudioPlayer _player;

  ConcatenatingAudioSource _playlist = ConcatenatingAudioSource(children: []);
  int _addedCount = 0;

  @override
  void initState() {
    super.initState();
    _player = AudioPlayer();
    setState(() {
      _playlist = ConcatenatingAudioSource(children: [
        for (int i = 0; i < widget.storyLink!.length; i++)
          // ClippingAudioSource(
          //   // start: const Duration(seconds: 0),
          //   // end: const Duration(seconds: 90),
          //   child: AudioSource.uri(Uri.parse(
          //       "https://s3.amazonaws.com/scifri-episodes/scifri20181123-episode.mp3")),
          //   tag: MediaItem(
          //     id: '${_nextMediaId++}',
          //     album: "Science Friday",
          //     title: "A Salute To Head-Scratching Science (30 seconds)",
          //     artUri: Uri.parse(
          //         "https://media.wnyc.org/i/1400/1400/l/80/1/ScienceFriday_WNYCStudios_1400.jpg"),
          //   ),
          // ),
          AudioSource.uri(
            Uri.parse(widget.storyLink![i]["link"]),
            tag: MediaItem(
              id: '${_nextMediaId++}',
              //album: widget.storyName,
              title: widget.storyName!,
              artUri: Uri.parse(
                  "https://media.wnyc.org/i/1400/1400/l/80/1/ScienceFriday_WNYCStudios_1400.jpg"),
            ),
          ),
      ]);
    });
    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      statusBarColor: Colors.black,
    ));
    _init();
  }

  Future<void> _init() async {
    final session = await AudioSession.instance;
    await session.configure(const AudioSessionConfiguration.speech());
    // Listen to errors during playback.
    _player.playbackEventStream.listen((event) {},
        onError: (Object e, StackTrace stackTrace) {
      print('A stream error occurred: $e');
    });
    try {
      await _player.setAudioSource(_playlist);
    } catch (e, stackTrace) {
      // Catch load errors: 404, invalid url ...
      print("Error loading playlist: $e");
      print(stackTrace);
    }
  }

  @override
  void dispose() {
    _player.dispose();
    super.dispose();
  }

  Stream<PositionData> get _positionDataStream =>
      Rx.combineLatest3<Duration, Duration, Duration?, PositionData>(
          _player.positionStream,
          _player.bufferedPositionStream,
          _player.durationStream,
          (position, bufferedPosition, duration) => PositionData(
              position, bufferedPosition, duration ?? Duration.zero));

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(
                child: StreamBuilder<SequenceState?>(
                  stream: _player.sequenceStateStream,
                  builder: (context, snapshot) {
                    final state = snapshot.data;
                    if (state?.sequence.isEmpty ?? true) {
                      return const SizedBox();
                    }
                    final metadata = state!.currentSource!.tag as MediaItem;
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Center(
                                child:
                                    Image.network(metadata.artUri.toString())),
                          ),
                        ),
                        Text(metadata.album!,
                            style: Theme.of(context).textTheme.headline6),
                        Text(metadata.title),
                      ],
                    );
                  },
                ),
              ),
              ControlButtons(_player),
              StreamBuilder<PositionData>(
                stream: _positionDataStream,
                builder: (context, snapshot) {
                  final positionData = snapshot.data;
                  return SeekBar(
                    duration: positionData?.duration ?? Duration.zero,
                    position: positionData?.position ?? Duration.zero,
                    bufferedPosition:
                        positionData?.bufferedPosition ?? Duration.zero,
                    onChangeEnd: (newPosition) {
                      _player.seek(newPosition);
                    },
                  );
                },
              ),
              const SizedBox(height: 8.0),
              Row(
                children: [
                  StreamBuilder<LoopMode>(
                    stream: _player.loopModeStream,
                    builder: (context, snapshot) {
                      final loopMode = snapshot.data ?? LoopMode.off;
                      const icons = [
                        Icon(Icons.repeat, color: Colors.grey),
                        Icon(Icons.repeat, color: Colors.orange),
                        Icon(Icons.repeat_one, color: Colors.orange),
                      ];
                      const cycleModes = [
                        LoopMode.off,
                        LoopMode.all,
                        LoopMode.one,
                      ];
                      final index = cycleModes.indexOf(loopMode);
                      return IconButton(
                        icon: icons[index],
                        onPressed: () {
                          _player.setLoopMode(cycleModes[
                              (cycleModes.indexOf(loopMode) + 1) %
                                  cycleModes.length]);
                        },
                      );
                    },
                  ),
                  Expanded(
                    child: Text(
                      "Playlist",
                      style: Theme.of(context).textTheme.headline6,
                      textAlign: TextAlign.center,
                    ),
                  ),
                  StreamBuilder<bool>(
                    stream: _player.shuffleModeEnabledStream,
                    builder: (context, snapshot) {
                      final shuffleModeEnabled = snapshot.data ?? false;
                      return IconButton(
                        icon: shuffleModeEnabled
                            ? const Icon(Icons.shuffle, color: Colors.orange)
                            : const Icon(Icons.shuffle, color: Colors.grey),
                        onPressed: () async {
                          final enable = !shuffleModeEnabled;
                          if (enable) {
                            await _player.shuffle();
                          }
                          await _player.setShuffleModeEnabled(enable);
                        },
                      );
                    },
                  ),
                ],
              ),
              SizedBox(
                height: 240.0,
                child: StreamBuilder<SequenceState?>(
                  stream: _player.sequenceStateStream,
                  builder: (context, snapshot) {
                    final state = snapshot.data;
                    final sequence = state?.sequence ?? [];
                    return ReorderableListView(
                      onReorder: (int oldIndex, int newIndex) {
                        if (oldIndex < newIndex) newIndex--;
                        _playlist.move(oldIndex, newIndex);
                      },
                      children: [
                        for (var i = 0; i < sequence.length; i++)
                          Dismissible(
                            key: ValueKey(sequence[i]),
                            background: Container(
                              color: Colors.redAccent,
                              alignment: Alignment.centerRight,
                              child: const Padding(
                                padding: EdgeInsets.only(right: 8.0),
                                child: Icon(Icons.delete, color: Colors.white),
                              ),
                            ),
                            onDismissed: (dismissDirection) {
                              _playlist.removeAt(i);
                            },
                            child: Material(
                              color: i == state!.currentIndex
                                  ? Colors.grey.shade300
                                  : null,
                              child: ListTile(
                                title: Text(sequence[i].tag.title as String),
                                onTap: () {
                                  _player.seek(Duration.zero, index: i);
                                },
                              ),
                            ),
                          ),
                      ],
                    );
                  },
                ),
              ),
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton(
          child: const Icon(Icons.add),
          onPressed: () {
            _playlist.add(AudioSource.uri(
              Uri.parse("asset:///audio/nature.mp3"),
              tag: MediaItem(
                id: '${_nextMediaId++}',
                album: "Public Domain",
                title: "Nature Sounds ${++_addedCount}",
                artUri: Uri.parse(
                    "https://media.wnyc.org/i/1400/1400/l/80/1/ScienceFriday_WNYCStudios_1400.jpg"),
              ),
            ));
          },
        ),
      ),
    );
  }
}

class ControlButtons extends StatelessWidget {
  final AudioPlayer player;

  const ControlButtons(this.player, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        IconButton(
          icon: const Icon(Icons.volume_up),
          onPressed: () {
            showSliderDialog(
              context: context,
              title: "Adjust volume",
              divisions: 10,
              min: 0.0,
              max: 1.0,
              stream: player.volumeStream,
              onChanged: player.setVolume,
            );
          },
        ),
        StreamBuilder<SequenceState?>(
          stream: player.sequenceStateStream,
          builder: (context, snapshot) => IconButton(
            icon: const Icon(Icons.skip_previous),
            onPressed: player.hasPrevious ? player.seekToPrevious : null,
          ),
        ),
        StreamBuilder<PlayerState>(
          stream: player.playerStateStream,
          builder: (context, snapshot) {
            final playerState = snapshot.data;
            final processingState = playerState?.processingState;
            final playing = playerState?.playing;
            if (processingState == ProcessingState.loading ||
                processingState == ProcessingState.buffering) {
              return Container(
                margin: const EdgeInsets.all(8.0),
                width: 64.0,
                height: 64.0,
                child: const CircularProgressIndicator(),
              );
            } else if (playing != true) {
              return IconButton(
                icon: const Icon(Icons.play_arrow),
                iconSize: 64.0,
                onPressed: player.play,
              );
            } else if (processingState != ProcessingState.completed) {
              return IconButton(
                icon: const Icon(Icons.pause),
                iconSize: 64.0,
                onPressed: player.pause,
              );
            } else {
              return IconButton(
                icon: const Icon(Icons.replay),
                iconSize: 64.0,
                onPressed: () => player.seek(Duration.zero,
                    index: player.effectiveIndices!.first),
              );
            }
          },
        ),
        StreamBuilder<SequenceState?>(
          stream: player.sequenceStateStream,
          builder: (context, snapshot) => IconButton(
            icon: const Icon(Icons.skip_next),
            onPressed: player.hasNext ? player.seekToNext : null,
          ),
        ),
        StreamBuilder<double>(
          stream: player.speedStream,
          builder: (context, snapshot) => IconButton(
            icon: Text("${snapshot.data?.toStringAsFixed(1)}x",
                style: const TextStyle(fontWeight: FontWeight.bold)),
            onPressed: () {
              showSliderDialog(
                context: context,
                title: "Adjust speed",
                divisions: 10,
                min: 0.5,
                max: 1.5,
                stream: player.speedStream,
                onChanged: player.setSpeed,
              );
            },
          ),
        ),
      ],
    );
  }
}
