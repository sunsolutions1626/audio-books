import 'dart:convert';
import 'package:audio_book/Notification/local_notification.dart';
import 'package:audio_book/Screens/audio_list.dart';
import 'package:audio_book/Widget/search_widget.dart';
import 'package:audio_book/Widget/sidebar.dart';
import 'package:audio_book/constance/AppColor.dart';
import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  var data;
  String query = "";
  FirebaseRemoteConfig remoteConfig = FirebaseRemoteConfig.instance;
  List<String> imageList = [
    "assets/images/By_The_Pricking_Of_My_Thumbs.png",
    "assets/images/Murder_on_the_Orient_Express.png",
    "assets/images/The_Labour_of_Hercules.png",
    "assets/images/Poirot's_Early_Cases.png",
    "assets/images/Three_Act_Tragedy.png",
    "assets/images/Death_in_Clouds.png",
    "assets/images/Adventure_of_the_Chritmas_pudding.png",
    "assets/images/Lord_Edgware_Dies.png",
    "assets/images/Crooked_House.png",
    "assets/images/After_the_Funeral.png",
  ];

  List<Map<String, dynamic>> originalData = [];
  List<Map<String, dynamic>> filterData = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    fetchData();
    var rawData = remoteConfig.getAll()["crime_audio_book"];
    var map = json.decode(rawData?.asString() ?? "");
    for (var item in map["data"]) {
      originalData.add(item);
    }
    print(originalData.length);
    filterData = originalData;
    print(filterData[1]);
    NotificationData.showNotification();
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Scaffold(
        drawer: const SideBar(),
        appBar: AppBar(
          backgroundColor: AppColor.primaryColor,
          centerTitle: true,
          title: const Text("Learn English : Audio Storybook"),
        ),
        body: FutureBuilder<FirebaseRemoteConfig>(
          future: setupRemoteConfig(),
          builder: (BuildContext context,
              AsyncSnapshot<FirebaseRemoteConfig> snapshot) {
            return snapshot.hasData
                ? listOfAudiobook(
                    width: width,
                    height: height,
                    remoteConfig: snapshot.requireData)
                : Container();
          },
        ));
  }

  listOfAudiobook({width, height, required FirebaseRemoteConfig remoteConfig}) {
    return Column(
      children: [
        buildSearch(),
        const SizedBox(
          height: 20,
        ),
        _itemList(width, height, remoteConfig: remoteConfig)
      ],
    );
  }

  Expanded _itemList(double width, double height,
      {required FirebaseRemoteConfig remoteConfig}) {
    return Expanded(
      child: ListView.builder(
        itemCount: filterData.length,
        physics: const BouncingScrollPhysics(),
        itemBuilder: (context, index) {
          var item = filterData[index];
          // bookNameList = duplicateGetAllData[index]["book_name"];
          // print(bookNameList);
          return Padding(
            padding: const EdgeInsets.only(right: 15, bottom: 10),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  children: [
                    SizedBox(height: 15),
                    Container(
                      margin: const EdgeInsets.only(top: 5),
                      padding: const EdgeInsets.only(
                          left: 15, right: 15, top: 10, bottom: 10),
                      decoration: const BoxDecoration(
                          color: AppColor.primaryColor,
                          borderRadius: BorderRadius.only(
                              topRight: Radius.circular(20),
                              bottomRight: Radius.circular(20))),
                      child: Text(
                        "${index + 1}",
                        style: const TextStyle(color: TextColor.textColorWhite),
                      ),
                    ),
                  ],
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.push(context, MaterialPageRoute(
                      builder: (context) {
                        return AudioList(
                            audiobook_name: item["book_name"],
                            storyLink: item["audio_links"],
                            image: imageList[index]);
                      },
                    ));
                  },
                  child: Stack(
                    children: [
                      Container(
                        margin: const EdgeInsets.all(5),
                        height: height * .22,
                        width: width * .8,
                        decoration: BoxDecoration(
                          color: Colors.blue,
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(10),
                          child: Image.asset(
                            imageList[index],
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      Positioned(
                        bottom: 0,
                        right: 0,
                        left: 0,
                        child: Container(
                          padding: const EdgeInsets.only(bottom: 10, top: 10),
                          margin: const EdgeInsets.all(5),
                          // height: 30,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              color: Colors.black.withOpacity(0.6),
                              borderRadius: const BorderRadius.only(
                                  bottomLeft: Radius.circular(10),
                                  bottomRight: Radius.circular(10))),
                          child: Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: Text(
                              item["book_name"],
                              style: const TextStyle(
                                  color: TextColor.textColorWhite,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          );
        },
      ),
    );
  }

  Widget buildSearch() => SearchWidget(
      text: "", onChanged: searchBook, hintText: "Title or Item Name");

  void searchBook(String query1) {
    final books1 = originalData.where((element) {
      final titleLower = element["book_name"].toLowerCase();
      final searchLower = query1.toLowerCase();
      return titleLower.contains(searchLower);
    }).toList();

    setState(() {
      filterData = books1;
    });
  }

  Future<FirebaseRemoteConfig> setupRemoteConfig() async {
    await remoteConfig.setConfigSettings(RemoteConfigSettings(
      fetchTimeout: const Duration(hours: 1),
      minimumFetchInterval: const Duration(minutes: 1),
    ));
    setState(() {});
    return remoteConfig;
  }

  Future<void> fetchData() async {
    await remoteConfig.fetchAndActivate();
  }
}
