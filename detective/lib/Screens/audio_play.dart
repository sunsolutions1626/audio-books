import 'dart:async';

import 'package:assets_audio_player/assets_audio_player.dart';
import 'package:audio_book/Screens/PlayingControls.dart';
import 'package:audio_book/Screens/seek.dart';
// import 'package:audio_book/Screens/songs_selector.dart';
import 'package:audio_book/Widget/controll_button.dart';
import 'package:audio_book/common/common.dart';
import 'package:audio_book/constance/AppColor.dart';
import 'package:audio_book/model/database%20model.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
// import 'package:just_audio/just_audio.dart';
// import 'package:just_audio_background/just_audio_background.dart';
import 'package:rxdart/rxdart.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sqflite/sqflite.dart';

import '../common/utils.dart';
import 'admob_helper.dart';

class AudioPlay extends StatefulWidget {
  String? storyName;
  List<dynamic>? storyLink;
  String? songUrl;
  String? image;
  String? storyID;

  AudioPlay(
      {this.storyName, this.songUrl, this.image, this.storyID, this.storyLink});

  @override
  State<AudioPlay> createState() => _AudioPlayState();
}

class _AudioPlayState extends State<AudioPlay> with WidgetsBindingObserver {
  //final _player = AudioPlayer();
  bool isFavorite = false;
  Database? database;
  String qry = "";
  List<Map> list = [];

  @override
  final AssetsAudioPlayer audioPlayer = AssetsAudioPlayer();

  late AssetsAudioPlayer _assetsAudioPlayer;
  final List<StreamSubscription> _subscriptions = [];
  final audios = <Audio>[];
  void getAllData() {
    ModelDatabase.createDataBase().then((value) async {
      database = value;
      qry = "select * from detectivetable WHERE storyID = ${widget.storyID}";
      list = await database!.rawQuery(qry);
      if (list.length > 0) {
        isFavorite = true;
      } else {
        isFavorite = false;
      }
      setState(() {});
    });
  }

  @override
  bool v = false;
  addStringToSF() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String count = prefs.getInt("count").toString();
    print(count);
    if (count == 'null') {
      count = "0";
      prefs.setInt("count", 1);
    } else if (int.parse(count) < 5) {
      int c = int.parse(count);
      prefs.setInt("count", c + 1);
    } else {
      setState(() {
        v = true;
      });
      prefs.setInt("count", 1);
    }
  }

  void initState() {
    _loadInterstitialAd();
    addStringToSF();
    super.initState();
    audioList.clear();
    print(widget.storyID);
    _assetsAudioPlayer = AssetsAudioPlayer.newPlayer();
    //_subscriptions.add(_assetsAudioPlayer.playlistFinished.listen((data) {
    //  print('finished : $data');
    //}));
    //openPlayer();
    _subscriptions.add(_assetsAudioPlayer.playlistAudioFinished.listen((data) {
      print('playlistAudioFinished : $data');
    }));
    _subscriptions.add(_assetsAudioPlayer.audioSessionId.listen((sessionId) {
      print('audioSessionId : $sessionId');
    }));
    //ambiguate(WidgetsBinding.instance)!.addObserver(this);

    ModelDatabase.createDataBase().then((value) => {database = value});
    getAllData();
    print("k");
    print(widget.storyLink!.length);
    for (int i = 0; i < widget.storyLink!.length; i++) {
      print(widget.storyLink![i]["id"]);
      setState(() {
        audioList.add(widget.storyLink![i]["link"]);

        audios.add(
          Audio.network(
            audioList[i],
            metas: Metas(
              id: widget.storyLink![i]["id"],
              title: widget.storyName,

              // artist: 'Florent Champigny',
              // album: 'OnlineAlbum',
              // image: MetasImage.network('https://www.google.com')
              image: MetasImage.asset(widget.image!),
            ),
          ),
        );
      });
    }
    openPlayer();

    initConnectivity();

    _connectivitySubscription =
        _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
  }

  @override
  void dispose() {
    _connectivitySubscription.cancel();
    _assetsAudioPlayer.dispose();
    super.dispose();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initConnectivity() async {
    late ConnectivityResult result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      // developer.log('Couldn\'t check connectivity status', error: e);
      return;
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return Future.value(null);
    }

    return _updateConnectionStatus(result);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    setState(() {
      _connectionStatus = result;
    });
  }

  ConnectivityResult _connectionStatus = ConnectivityResult.none;
  final Connectivity _connectivity = Connectivity();
  late StreamSubscription<ConnectivityResult> _connectivitySubscription;

  static List audioList = [];
  void openPlayer() async {
    await _assetsAudioPlayer.open(
      Playlist(
          audios: audios, startIndex: int.parse(widget.storyID.toString()) - 1),
      showNotification: true,
      autoStart: false,
      headPhoneStrategy: HeadPhoneStrategy.pauseOnUnplug,
      playInBackground: PlayInBackground.enabled,
    );
  }

  Audio find(List<Audio> source, String fromPath) {
    return source.firstWhere((element) => element.path == fromPath);
  }

  InterstitialAd? _interstitialAd;

  // TODO: Implement _loadInterstitialAd()
  void _loadInterstitialAd() {
    InterstitialAd.load(
      adUnitId: AdHelper.interstitialAdUnitId,
      request: AdRequest(),
      adLoadCallback: InterstitialAdLoadCallback(
        onAdLoaded: (ad) {
          ad.fullScreenContentCallback = FullScreenContentCallback(
            onAdDismissedFullScreenContent: (ad) {
              AudioPlay();
            },
          );

          setState(() {
            _interstitialAd = ad;
          });
        },
        onAdFailedToLoad: (err) {
          print('Failed to load an interstitial ad: ${err.message}');
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    if (v == true) {
      _interstitialAd?.show();
    }
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: AppColor.primaryColor,
        title: Text("Story ${widget.storyName.toString()}",
            style: TextStyle(fontSize: 15)),
        actions: [
          Stack(
            children: [
              Padding(
                  padding: const EdgeInsets.only(right: 20),
                  child: Center(
                    child: GestureDetector(
                        onTap: () async {
                          isFavorite = !isFavorite;
                          if (isFavorite) {
                            String qry =
                                "insert into detectivetable (image,storyName,storyLink,storyID) values ('${widget.image}','${widget.storyName.toString()}','${widget.songUrl}','${widget.storyID}')";
                            await database!.rawInsert(qry);
                            setState(() {});
                          } else {
                            String qry =
                                "DELETE FROM detectivetable WHERE storyID = ${widget.storyID}";
                            await database!.rawDelete(qry);
                            setState(() {});
                          }
                        },
                        child: Image.asset(
                            color: !isFavorite
                                ? Color(0xffEEEB6A3)
                                : Color(0xffD9592F),
                            !isFavorite
                                ? "assets/images/black_leart.png"
                                : "assets/images/red_heart.png",
                            width: 25,
                            height: 25)),
                  )),
            ],
          ),
        ],
      ),
      body: Stack(children: [
        Scrollbar(
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Container(
                  width: width,
                  height: height,
                  alignment: Alignment.center,
                  child: const Text("")),
            ),
          ),
        ),
        Positioned(
          bottom: 0,
          left: 0,
          right: 0,
          child: Container(
            height: height * .23,
            color: AppColor.primaryColor,
            child: Column(
              children: [
                Padding(
                  padding: EdgeInsets.only(top: height * .02),

                  child: _assetsAudioPlayer.builderCurrent(
                      builder: (context, Playing? playing) {
                    return Column(
                      children: <Widget>[
                        _assetsAudioPlayer.builderLoopMode(
                          builder: (context, loopMode) {
                            return PlayerBuilder.isPlaying(
                                player: _assetsAudioPlayer,
                                builder: (context, isPlaying) {
                                  return PlayingControls(
                                    loopMode: loopMode,
                                    isPlaying: isPlaying,
                                    isPlaylist: true,
                                    onStop: () {
                                      _assetsAudioPlayer.stop();
                                    },
                                    toggleLoop: () {
                                      _assetsAudioPlayer.toggleLoop();
                                    },
                                    onPlay: () {
                                      _assetsAudioPlayer.playOrPause();
                                    },
                                    onNext: () {
                                      //_assetsAudioPlayer.forward(Duration(seconds: 10));
                                      _assetsAudioPlayer.next(
                                          stopIfLast: false,
                                          keepLoopMode: true);
                                    },
                                    onPrevious: () {
                                      _assetsAudioPlayer.previous(
                                          /*keepLoopMode: false*/);
                                    },
                                  );
                                });
                          },
                        ),
                        _assetsAudioPlayer.builderRealtimePlayingInfos(
                            builder: (context, RealtimePlayingInfos? infos) {
                          if (infos == null) {
                            return const CircularProgressIndicator(
                              color: Colors.black,
                              strokeWidth: 10,
                            );
                          }
                          print('infos: $infos');
                          return Column(
                            children: [
                              PositionSeekWidget(
                                currentPosition: infos.currentPosition,
                                duration: infos.duration,
                                seekTo: (to) {
                                  _assetsAudioPlayer.seek(to);
                                },
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  NeumorphicButton(
                                    style: const NeumorphicStyle(
                                        color: Colors.white),
                                    onPressed: () {
                                      _assetsAudioPlayer
                                          .seekBy(const Duration(minutes: -10));
                                    },
                                    child: const Text('-10m'),
                                  ),
                                  const SizedBox(
                                    width: 12,
                                  ),
                                  NeumorphicButton(
                                    style: const NeumorphicStyle(
                                        color: Colors.white),
                                    onPressed: () {
                                      _assetsAudioPlayer
                                          .seekBy(const Duration(seconds: -10));
                                    },
                                    child: const Text('-10s'),
                                  ),
                                  const SizedBox(
                                    width: 12,
                                  ),
                                  NeumorphicButton(
                                    style: const NeumorphicStyle(
                                        color: Colors.white),
                                    onPressed: () {
                                      _assetsAudioPlayer
                                          .seekBy(const Duration(seconds: 10));
                                    },
                                    child: const Text('+10s'),
                                  ),
                                  const SizedBox(
                                    width: 12,
                                  ),
                                  NeumorphicButton(
                                    style: const NeumorphicStyle(
                                        color: Colors.white),
                                    onPressed: () {
                                      _assetsAudioPlayer
                                          .seekBy(const Duration(minutes: 10));
                                    },
                                    child: const Text('+10m'),
                                  ),
                                ],
                              )
                            ],
                          );
                        }),
                      ],
                    );
                  }),

                  // _assetsAudioPlayer.builderCurrent(
                  //     builder: (BuildContext context, Playing? playing) {
                  //   return SongsSelector(
                  //     audios: audios,
                  //     onPlaylistSelected: (myAudios) {
                  //       _assetsAudioPlayer.open(
                  //         Playlist(audios: myAudios),
                  //         showNotification: true,
                  //         headPhoneStrategy:
                  //             HeadPhoneStrategy.pauseOnUnplugPlayOnPlug,
                  //         audioFocusStrategy: AudioFocusStrategy.request(
                  //             resumeAfterInterruption: true),
                  //       );
                  //     },
                  //     onSelected: (myAudio) async {
                  //       try {
                  //         await _assetsAudioPlayer.open(
                  //           myAudio,
                  //           autoStart: true,
                  //           showNotification: true,
                  //           playInBackground: PlayInBackground.enabled,
                  //           audioFocusStrategy: AudioFocusStrategy.request(
                  //               resumeAfterInterruption: true,
                  //               resumeOthersPlayersAfterDone: true),
                  //           headPhoneStrategy: HeadPhoneStrategy.pauseOnUnplug,
                  //           notificationSettings: NotificationSettings(
                  //               //seekBarEnabled: false,
                  //               //stopEnabled: true,
                  //               //customStopAction: (player){
                  //               //  player.stop();
                  //               //}
                  //               //prevEnabled: false,
                  //               //customNextAction: (player) {
                  //               //  print('next');
                  //               //}
                  //               //customStopIcon: AndroidResDrawable(name: 'ic_stop_custom'),
                  //               //customPauseIcon: AndroidResDrawable(name:'ic_pause_custom'),
                  //               //customPlayIcon: AndroidResDrawable(name:'ic_play_custom'),
                  //               ),
                  //         );
                  //       } catch (e) {
                  //         print(e);
                  //       }
                  //     },
                  //     playing: playing,
                  //   );
                  // }),
                  /*
                  PlayerBuilder.volume(
                      player: _assetsAudioPlayer,
                      builder: (context, volume) {
                        return VolumeSelector(
                          volume: volume,
                          onChange: (v) {
                            _assetsAudioPlayer.setVolume(v);
                          },
                        );
                      }),
                   */
                  /*
                  PlayerBuilder.forwardRewindSpeed(
                      player: _assetsAudioPlayer,
                      builder: (context, speed) {
                        return ForwardRewindSelector(
                          speed: speed,
                          onChange: (v) {
                            _assetsAudioPlayer.forwardOrRewind(v);
                          },
                        );
                      }),
                   */
                  /*
                  PlayerBuilder.playSpeed(
                      player: _assetsAudioPlayer,
                      builder: (context, playSpeed) {
                        return PlaySpeedSelector(
                          playSpeed: playSpeed,
                          onChange: (v) {
                            _assetsAudioPlayer.setPlaySpeed(v);
                          },
                        );
                      }),
                   */
                )
              ],
            ),
          ),
        ),
        _connectionStatus.toString().contains("none") ? internet() : Container()
      ]),
    );
  }
}
