import 'package:audio_book/Screens/favrouit_screen.dart';
import 'package:flutter/material.dart';

class SideBar extends StatefulWidget {
  const SideBar({Key? key}) : super(key: key);

  @override
  State<SideBar> createState() => _SideBarState();
}

class _SideBarState extends State<SideBar> {
  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;

    return Drawer(
      child: Stack(
        children: [
          Image.asset(
            "assets/images/drawer_pannel.jpg",
            height: height,
            fit: BoxFit.cover,
          ),
          Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            const Padding(
              padding: EdgeInsets.only(left: 30, top: 70),
              child: CircleAvatar(
                radius: 50,
                backgroundColor: Colors.black,
              ),
            ),
            const SizedBox(height: 40),
            listWidget(
              text: "Favorite",
              next: "assets/images/ic_heart.PNG",
              color: Colors.red,
              onTap: () {
                Navigator.push(context, MaterialPageRoute(
                  builder: (context) {
                    return FavouriteScreen();
                  },
                ));
              },
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20),
              child:
                  Container(color: Colors.grey.shade400, width: 200, height: 1),
            ),
            listWidget(
                text: "Share App",
                next: "assets/images/ic_share_new.PNG",
                color: Colors.black),
            Padding(
              padding: const EdgeInsets.only(left: 20),
              child:
                  Container(color: Colors.grey.shade400, width: 200, height: 1),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20),
              child:
                  Container(color: Colors.grey.shade400, width: 200, height: 1),
            ),
            listWidget(
                text: "Rate us",
                next: "assets/images/ic_rate.PNG",
                color: Colors.amber),
            Padding(
              padding: const EdgeInsets.only(left: 20),
              child:
                  Container(color: Colors.grey.shade400, width: 200, height: 1),
            ),
            listWidget(
                text: "Privacy Policy",
                next: "assets/images/ic_policy.PNG",
                color: null),
            Padding(
              padding: const EdgeInsets.only(left: 20),
              child:
                  Container(color: Colors.grey.shade400, width: 200, height: 1),
            ),
            listWidget(
                text: "More App",
                next: "assets/images/ic_app.PNG",
                color: null),
            Padding(
              padding: const EdgeInsets.only(left: 20),
              child:
                  Container(color: Colors.grey.shade400, width: 200, height: 1),
            ),
            listWidget(
                text: "Remove Ads",
                next: "assets/images/ic_baseline_not_interested_24.PNG",
                color: Colors.red),
          ]),
        ],
      ),
    );
  }

  Widget listWidget(
      {String? text, String? next, Color? color, VoidCallback? onTap}) {
    return GestureDetector(
      onTap: onTap,
      child: ListTile(
        title: Text(
          text!,
          style: const TextStyle(
            fontSize: 18,
            color: Colors.black,
          ),
        ),
        leading: Image.asset(
          next!,
          color: color,
          height: 30,
          width: 30,
          //size: 35,
        ),
      ),
    );
  }
}
