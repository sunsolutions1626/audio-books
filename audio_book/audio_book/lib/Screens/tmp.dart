// import 'package:audio_book/Widget/controll_button.dart';
// import 'package:audio_book/common/common.dart';
// import 'package:audio_book/constance/AppColor.dart';
// import 'package:audio_book/model/database%20model.dart';
// import 'package:flutter/material.dart';
// import 'package:just_audio/just_audio.dart';
// import 'package:rxdart/rxdart.dart';
// import 'package:sqflite/sqflite.dart';

// class AudioPlay extends StatefulWidget {
//   String? storyName;
//   String? songUrl;
//   String? image;
//   String? storyID;

//   AudioPlay({this.storyName, this.songUrl, this.image, this.storyID});

//   @override
//   State<AudioPlay> createState() => _AudioPlayState();
// }

// class _AudioPlayState extends State<AudioPlay> with WidgetsBindingObserver {
//   final _player = AudioPlayer();
//   bool isFavorite = false;
//   Database? database;
//   String qry = "";
//   List<Map> list = [];

//   @override
//   void initState() {
//     super.initState();
//     ambiguate(WidgetsBinding.instance)!.addObserver(this);
//     _init();
//     ModelDatabase.createDataBase().then((value) => {database = value});
//     getAllData();
//   }

//   Future<void> _init() async {
//     try {
//       await _player.setAudioSource(AudioSource.uri(Uri.parse(widget.songUrl!)));
//     } catch (e) {
//       print("Error loading audio source: $e");
//     }
//   }

//   @override
//   void dispose() {
//     ambiguate(WidgetsBinding.instance)!.removeObserver(this);
//     _player.dispose();
//     super.dispose();
//   }

//   @override
//   void didChangeAppLifecycleState(AppLifecycleState state) {
//     if (state == AppLifecycleState.paused) {
//       _player.stop();
//     }
//   }

//   Stream<PositionData> get _positionDataStream =>
//       Rx.combineLatest3<Duration, Duration, Duration?, PositionData>(
//           _player.positionStream,
//           _player.bufferedPositionStream,
//           _player.durationStream,
//           (position, bufferedPosition, duration) => PositionData(
//               position, bufferedPosition, duration ?? Duration.zero));

//   void getAllData() {
//     ModelDatabase.createDataBase().then((value) async {
//       database = value;
//       qry = "select * from addInCart WHERE storyID = ${widget.storyID}";
//       list = await database!.rawQuery(qry);
//       if (list.length > 0) {
//         isFavorite = true;
//       } else {
//         isFavorite = false;
//       }
//       setState(() {});
//     });
//   }

//   @override
//   Widget build(BuildContext context) {
//     var width = MediaQuery.of(context).size.width;
//     var height = MediaQuery.of(context).size.height;
//     return Scaffold(
//       appBar: AppBar(
//         centerTitle: true,
//         backgroundColor: AppColor.primaryColor,
//         title: Text("story ${widget.storyName.toString()}"),
//         actions: [
//           Padding(
//               padding: const EdgeInsets.only(right: 20),
//               child: GestureDetector(
//                   onTap: () async {
//                     isFavorite = !isFavorite;
//                     if (isFavorite) {
//                       String qry =
//                           "insert into addInCart (image,storyName,storyLink,storyID) values ('${widget.image}','${widget.storyName.toString()}','${widget.songUrl}','${widget.storyID}')";
//                       await database!.rawInsert(qry);
//                       setState(() {});
//                     } else {
//                       String qry =
//                           "DELETE FROM addInCart WHERE storyID = ${widget.storyID}";
//                       await database!.rawDelete(qry);
//                       setState(() {});
//                     }
//                   },
//                   child: Image.asset(
//                       !isFavorite
//                           ? "assets/images/black_leart.png"
//                           : "assets/images/red_heart.png",
//                       width: 30,
//                       height: 30))),
//         ],
//       ),
//       body: Stack(
//         children: [
//           Scrollbar(
//             child: SingleChildScrollView(
//               child: Padding(
//                 padding: const EdgeInsets.symmetric(horizontal: 20),
//                 child: Container(
//                     width: width,
//                     height: height,
//                     alignment: Alignment.center,
//                     child: const Text("")),
//               ),
//             ),
//           ),
//           Positioned(
//             bottom: 0,
//             left: 0,
//             right: 0,
//             child: Container(
//               height: height * .2,
//               color: AppColor.primaryColor,
//               child: Column(
//                 children: [
//                   Padding(
//                     padding: EdgeInsets.only(top: height * .02),
//                     child: StreamBuilder<PositionData>(
//                       stream: _positionDataStream,
//                       builder: (context, snapshot) {
//                         final positionData = snapshot.data;
//                         return SeekBar(
//                           duration: positionData?.duration ?? Duration.zero,
//                           position: positionData?.position ?? Duration.zero,
//                           bufferedPosition:
//                               positionData?.bufferedPosition ?? Duration.zero,
//                           onChangeEnd: _player.seek,
//                         );
//                       },
//                     ),
//                   ),
//                   ControlButtons(_player),
//                 ],
//               ),
//             ),
//           ),
//         ],
//       ),
//     );
//   }
// }
