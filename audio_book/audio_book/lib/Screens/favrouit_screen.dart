import 'package:audio_book/Screens/audio_play.dart';
import 'package:audio_book/model/database%20model.dart';
import 'package:flutter/material.dart';
import 'package:sqflite/sqflite.dart';

import '../constance/AppColor.dart';

class FavouriteScreen extends StatefulWidget {
  const FavouriteScreen({Key? key}) : super(key: key);

  @override
  State<FavouriteScreen> createState() => _FavouriteScreenState();
}

class _FavouriteScreenState extends State<FavouriteScreen> {

  List<Map> list = [];
  Database? database;
  String? qry;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // getData();
    getAllData();
  }

  // void getData() {
  //   ModelDatabase.createDataBase().then((value) {
  //     database = value;
  //   });
  // }

  void getAllData() {
    ModelDatabase.createDataBase().then((value) async {
      database = value;
      qry = "select * from addInCart";
      list = await database!.rawQuery(qry!);
      print(list);
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColor.primaryColor,
        centerTitle: true,
        title: const Text("My Favorite"),
      ),
      body: Padding(
        padding: const EdgeInsets.only(top: 20),
        child: ListView.builder(
          physics: const BouncingScrollPhysics(),
          itemCount: list.length,
          itemBuilder: (context, index) {
            return Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  padding: const EdgeInsets.only(
                      left: 20, right: 20, top: 10, bottom: 10),
                  decoration: const BoxDecoration(
                      color: AppColor.primaryColor,
                      borderRadius: BorderRadius.only(
                          topRight: Radius.circular(20),
                          bottomRight: Radius.circular(20))),
                  child: Text("${index + 1}",style:const TextStyle(color: Colors.white),),
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.push(context, MaterialPageRoute(builder: (context) {
                      return AudioPlay(storyName: "${index + 1}",
                        songUrl: list[index]["storyLink"],
                        storyID: list[index]["storyID"],
                        );
                    },));
                  },child: _imageClick(height, width, index,list[index]["image"]))
              ],
            );
          },),
      ),
    );
  }

Stack _imageClick(double height, double width, int index,String? image) {
  return Stack(
    children: [
      Container(
        margin: const EdgeInsets.all(5),
        height: height * .18,
        width: width * .8,
        decoration: BoxDecoration(
          color: Colors.blue,
          borderRadius: BorderRadius.circular(10),
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(10),
          child: Image.asset(
            image!,
            fit: BoxFit.cover,
          ),
        ),
      ),
      Positioned(
          bottom: 0,
          right: 0,
          left: 0,
          child: Container(
            margin: const EdgeInsets.all(5),
            height: 30,
            alignment: Alignment.center,
            decoration: BoxDecoration(
                color: Colors.black.withOpacity(0.6),
                borderRadius: const BorderRadius.only(
                    bottomLeft: Radius.circular(10),
                    bottomRight: Radius.circular(10))),
            child: Text(
              "Story ${index + 1}",
              style: const TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 18),
            ),
          ))
    ],
  );
}
}
