import 'package:assets_audio_player/assets_audio_player.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';

import 'assets_audio_icons.dart';

class PlayingControls extends StatelessWidget {
  final bool isPlaying;
  final LoopMode? loopMode;
  final bool isPlaylist;
  final Function()? onPrevious;
  final Function() onPlay;
  final Function()? onNext;
  final Function()? toggleLoop;
  final Function()? onStop;

  PlayingControls({
    required this.isPlaying,
    this.isPlaylist = false,
    this.loopMode,
    this.toggleLoop,
    this.onPrevious,
    required this.onPlay,
    this.onNext,
    this.onStop,
  });

  Widget _loopIcon(BuildContext context) {
    final iconSize = 34.0;
    if (loopMode == LoopMode.none) {
      return Icon(
        Icons.loop,
        size: iconSize,
        color: Colors.grey,
      );
    } else if (loopMode == LoopMode.playlist) {
      return Icon(
        Icons.loop,
        size: iconSize,
        color: Colors.black,
      );
    } else {
      //single
      return Stack(
        alignment: Alignment.center,
        children: [
          Icon(
            Icons.loop,
            size: iconSize,
            color: Colors.black,
          ),
          Center(
            child: Text(
              '1',
              style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold),
            ),
          ),
        ],
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        IconButton(
            icon: const Icon(Icons.skip_previous_outlined,
                size: 34, color: Colors.white),
            onPressed: () {
              isPlaylist ? onPrevious!() : null;
            }),
        IconButton(
          icon: Icon(
            !isPlaying ? Icons.play_arrow : Icons.pause,
            color: Colors.white,
            size: 34,
          ),
          onPressed: () {
            onPlay();
          },
        ),
        IconButton(
            icon: const Icon(Icons.skip_next_outlined,
                size: 34, color: Colors.white),
            onPressed: () {
              isPlaylist ? onNext!() : null;
            }),

        // if (onStop != null)
        //   NeumorphicButton(
        //     style: NeumorphicStyle(
        //       boxShape: NeumorphicBoxShape.circle(),
        //     ),
        //     padding: EdgeInsets.all(16),
        //     onPressed: onStop,
        //     child: Icon(
        //       AssetAudioPlayerIcons.stop,
        //       size: 32,
        //     ),
        //   ),
      ],
    );
  }
}
