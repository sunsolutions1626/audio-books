import 'package:flutter/material.dart';

class AppColor {
  static const primaryColor = Color(0xff154c79);
  static const darkPrimary = Color(0xff3A393E);
  static const accentColor = Color(0xff2aa180);
}

class TextColor {
  static const textColorWhite = Color(0xffffffff);
  static const textColorBlack = Color(0xff424242);
}
