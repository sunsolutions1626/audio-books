import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class ModelDatabase {
  static Future<Database> createDataBase() async {
    var databasesPath = await getDatabasesPath();
    String path = join(databasesPath, 'detectivetdb.db');

    Database database = await openDatabase(path, version: 1,
        onCreate: (Database db, int version) async {
      await db.execute(
          'CREATE TABLE detectivetable (id INTEGER PRIMARY KEY AUTOINCREMENT, image TEXT, storyName TEXT, storyLink TEXT, storyID TEXT)');
    });
    return database;
  }
}
