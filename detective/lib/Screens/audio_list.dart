import 'dart:async';

import 'package:audio_book/Screens/audio_play.dart';
import 'package:audio_book/common/utils.dart';
import 'package:audio_book/constance/AppColor.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:just_audio/just_audio.dart';

class AudioList extends StatefulWidget {
  String? audiobook_name;
  List<dynamic>? storyLink;
  String? image;

  List<String>? storyID;

  AudioList({this.audiobook_name, this.storyLink, this.image, this.storyID});

  @override
  State<AudioList> createState() => _AudioListState();
}

class _AudioListState extends State<AudioList> {
  final AudioPlayer _audioPlayer = AudioPlayer();

  List<Map<String, dynamic>> originalData = [];
  List<Map<String, dynamic>> filterData = [];
  @override
  initState() {
    // TODO: implement initState
    super.initState();

    initConnectivity();

    _connectivitySubscription =
        _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
  }

  void dispose() {
    _connectivitySubscription.cancel();
    super.dispose();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initConnectivity() async {
    late ConnectivityResult result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      // developer.log('Couldn\'t check connectivity status', error: e);
      return;
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return Future.value(null);
    }

    return _updateConnectionStatus(result);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    setState(() {
      _connectionStatus = result;
    });
  }

  ConnectivityResult _connectionStatus = ConnectivityResult.none;
  final Connectivity _connectivity = Connectivity();
  late StreamSubscription<ConnectivityResult> _connectivitySubscription;

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Scaffold(
        appBar: AppBar(
          backgroundColor: AppColor.primaryColor,
          title:
              Text(widget.audiobook_name ?? "", style: TextStyle(fontSize: 15)),
        ),
        body: Stack(
          children: [
            Padding(
              padding: const EdgeInsets.only(
                top: 20,
              ),
              child: ListView.builder(
                itemCount: widget.storyLink!.length,
                physics: const BouncingScrollPhysics(),
                itemBuilder: (context, index) {
                  return GestureDetector(
                    onTap: () {
                      print("audiolidt00");
                      print(widget.storyLink![index]["id"]);
                      Navigator.push(context, MaterialPageRoute(
                        builder: (context) {
                          return AudioPlay(
                            image: widget.image,
                            storyLink: widget.storyLink,
                            songUrl: widget.storyLink![index]["link"],
                            storyName: widget.audiobook_name,
                            storyID: widget.storyLink![index]["id"],
                          );
                        },
                      ));
                    },
                    child: Padding(
                      padding: const EdgeInsets.only(right: 25, bottom: 10),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            children: [
                              SizedBox(height: 15),
                              Container(
                                margin: const EdgeInsets.only(top: 5),
                                padding: const EdgeInsets.only(
                                    left: 15, right: 15, top: 10, bottom: 10),
                                decoration: const BoxDecoration(
                                    color: Color(0xffBC6621),
                                    borderRadius: BorderRadius.only(
                                        topRight: Radius.circular(20),
                                        bottomRight: Radius.circular(20))),
                                child: Text(
                                  "${index + 1}",
                                  style: const TextStyle(
                                      color: TextColor.textColorWhite),
                                ),
                              ),
                            ],
                          ),
                          Stack(
                            children: [
                              Container(
                                margin: const EdgeInsets.all(5),
                                height: height * .2,
                                width: width * .72,
                                decoration: BoxDecoration(
                                  boxShadow: [
                                    BoxShadow(
                                        offset: Offset(0, 4),
                                        color: Colors.black,
                                        blurRadius: 8)
                                  ],
                                  color: Colors.blue,
                                  borderRadius: BorderRadius.circular(5),
                                ),
                                child: ClipRRect(
                                    borderRadius: BorderRadius.circular(5),
                                    child: Image.asset(
                                      widget.image ?? "",
                                      fit: BoxFit.cover,
                                    )),
                              ),
                              Positioned(
                                  bottom: 0,
                                  right: 0,
                                  left: 0,
                                  child: Container(
                                    padding: const EdgeInsets.only(
                                        bottom: 10, top: 10),
                                    margin: const EdgeInsets.all(5),
                                    alignment: Alignment.center,
                                    decoration: BoxDecoration(
                                        color: Colors.black.withOpacity(0.6),
                                        borderRadius: const BorderRadius.only(
                                            bottomLeft: Radius.circular(5),
                                            bottomRight: Radius.circular(5))),
                                    child: Text(
                                      "Story ${index + 1}",
                                      style: const TextStyle(
                                          color: TextColor.textColorWhite,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 18),
                                    ),
                                  ))
                            ],
                          ),
                        ],
                      ),
                    ),
                  );
                },
              ),
            ),
            _connectionStatus.toString().contains("none")
                ? internet()
                : Container()
          ],
        ));
  }
}
