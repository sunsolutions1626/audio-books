import 'dart:io';

import 'package:flutter/material.dart';

internet() {
  return Positioned.fill(
      child: Center(
    child: Container(
      color: Colors.black54,
      height: double.infinity,
      width: double.infinity,
      alignment: Alignment.center,
      child: Padding(
        padding: const EdgeInsets.only(left: 30, right: 30),
        child: Container(
          width: double.infinity,
          padding: EdgeInsets.all(5),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(7), color: Colors.white),
          child: Wrap(
            children: [
              Container(height: 7),
              Text(
                  "Internet require for stream audiobook, Please enable internet and try again!!",
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 15, fontWeight: FontWeight.w500)),
              Container(height: 20),
              GestureDetector(
                onTap: () {
                  exit(0);
                },
                child: Center(
                  child: Text("GOT IT",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Colors.deepOrange,
                          fontSize: 15,
                          fontWeight: FontWeight.w600)),
                ),
              ),
              Container(height: 7),
            ],
          ),
        ),
      ),
    ),
  ));
}
